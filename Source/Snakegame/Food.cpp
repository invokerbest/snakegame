// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "MyGameInstance.h"
#include "Kismet/GameplayStatics.h"



// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
}
// Called every frame
void AFood::Tick(float DeltaTime)

{
	Super::Tick(DeltaTime);
	
	/*BTime += DeltaTime;
	if (BTime > StepDeley)
	{
		Destroy(true, true);
	}*/

}
void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			UMyGameInstance* Snakescore = Cast<UMyGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
			if (Snakescore)
			{
				Snakescore->Scoreadd();
			}
			Destroy();
			RandomFood();
		}
}
}

void AFood::RandomFood()
{


	float SpawnX = FMath::FRandRange(minX, maxX);
	float SpawnY = FMath::FRandRange(minY, maxY);

	FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);
	FTransform NewTransform(StartPoint);
	AFood* NewElem = GetWorld()->SpawnActor<AFood>(FoodElementClass, NewTransform);

}







/*void AFood::CollectEat(AActor* Interactor, bool bIsHead)
{
	
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			UMyGameInstance* Snakescore = Cast<UMyGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
			if (Snakescore)
			{
				Snakescore->Scoreadd();
			}
		}
	


}*/