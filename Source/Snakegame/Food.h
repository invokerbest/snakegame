// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Food.generated.h"

UCLASS()
class SNAKEGAME_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood>FoodElementClass;

	UPROPERTY()
		TArray<AFood*> FoodElements;

	UPROPERTY()
		TArray<AActor*> CollectedActors;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	void RandomFood();

	//void CollectEat(AActor* Interactor, bool bIsHead);

	

	//float StepDeley = 15.f;
	//float BTime = 0;
	float minX = -349.f;
	float maxX = 520.f;
	float minY = -434.f;
	float maxY = 434.f;
	float SpawnZ = -30.f;

};
