// Fill out your copyright notice in the Description page of Project Settings.

#include "MyGenereatorFood.h"
#include "Food.h"
#include "SnakeBase.h"


// Sets default values
AMyGenereatorFood::AMyGenereatorFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void AMyGenereatorFood::BeginPlay()
{
	Super::BeginPlay();
	RandomFood();
}

// Called every frame
void AMyGenereatorFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	/*BTime += DeltaTime;
	if (BTime > StepDelay)
	{
		RandomFood();
		BTime = 0;
	}*/
	
}

void AMyGenereatorFood::RandomFood()
{
	
	
	float SpawnX = FMath::FRandRange(minX, maxX);
	float SpawnY = FMath::FRandRange(minY, maxY);

	FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);
	FTransform NewTransform(StartPoint);
	AFood* NewElem = GetWorld()->SpawnActor<AFood>(FoodElementClass, NewTransform);
	
}