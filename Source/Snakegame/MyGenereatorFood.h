// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyGenereatorFood.generated.h"

class AFood;

UCLASS()
class SNAKEGAME_API AMyGenereatorFood : public AActor
{
	GENERATED_BODY()
	
		
public:	
	// Sets default values for this actor's properties
	AMyGenereatorFood();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood>FoodElementClass;

	UPROPERTY()
		TArray<AFood*> FoodElements;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	float minX = -349.f;
	float maxX = 520.f;
	float minY = -434.f;
	float maxY = 434.f;
	float SpawnZ = -30.f;
	//float StepDelay = 15.f;
	//float BTime = 0;

	void RandomFood();

};
