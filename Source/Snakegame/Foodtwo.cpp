// Fill out your copyright notice in the Description page of Project Settings.


#include "Foodtwo.h"
#include "SnakeBase.h"
#include "MyGameInstance.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AFoodtwo::AFoodtwo()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodtwo::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodtwo::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//CollectEat();


}

void AFoodtwo::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			UMyGameInstance* Snakescore = Cast<UMyGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
			if (Snakescore)
			{
				Snakescore->Scoreadd();
			}
			Destroy();
			RandomFood1();
		}
	}
}








void AFoodtwo::RandomFood1()
{


	float SpawnX = FMath::FRandRange(minX1, maxX1);
	float SpawnY = FMath::FRandRange(minY1, maxY1);

	FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ1);
	FTransform NewTransform(StartPoint);
	AFoodtwo* NewElem = GetWorld()->SpawnActor<AFoodtwo>(FoodElementClass1, NewTransform);

}