// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Bonus.generated.h"

UCLASS()
class SNAKEGAME_API ABonus : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonus();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool BnSpeed;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABonus>BNElementClass;

	UPROPERTY()
		TArray<ABonus*> Elements;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	

	float minX1 = -1170.f;
	float maxX1 = 960.f;
	float minY1 = -1610.f;
	float maxY1 = 1480.f;
	float SpawnZ1 = 30.f;
};
